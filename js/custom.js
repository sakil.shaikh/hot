$('.testimonial-slider').owlCarousel({
    loop:true,
    margin:10,
    nav: true,
    navText: ["<i class='fa-solid fa-arrow-left-long'></i>","<i class='fa-solid fa-arrow-right-long'></i>"],
    dots: false,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:1
        },
        1000:{
            items:1
        }
    }
});




$(document).mousemove(function(ev) {
    var positionX = (window.innerWidth / -90 - ev.pageX) / -70;
    var positionY = -ev.pageY / -50;
    $("#inner-content-img").css("transform", "translate(" + positionX + "px, " + positionY + "px)");
});
$(document).mousemove(function(ev2) {
    var positionX2 = (window.innerWidth / -10 - ev2.pageX) / -70;
    var positionY2 = -ev2.pageY / 50;
    $("#inner-bubble").css("transform", "translate(" + positionX2 + "px, " + positionY2 + "px)");
});
$(document).mousemove(function(ev3) {
    var positionX3 = (window.innerWidth / -130 - ev3.pageX) / -70;
    var positionY3 = -ev3.pageY / -90;
    $(".inner-abt-shadow").css("transform", "translate(" + positionX3 + "px, " + positionY3 + "px)");
});

$('.skill-slider').owlCarousel({
    loop:true,
    margin:10,
    nav: true,
    navText: ["<i class='fa-solid fa-arrow-left-long'></i>","<i class='fa-solid fa-arrow-right-long'></i>"],
    dots: false,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:1
        },
        1000:{
            items:1
        }
    }
});

$(document).ready(function(){
  $(".navbar-toggler").click(function(){
    $("body").toggleClass('s-open');
  });
});